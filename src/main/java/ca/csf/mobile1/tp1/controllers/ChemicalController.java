package ca.csf.mobile1.tp1.controllers;

import ca.csf.mobile1.tp1.models.ChemicalModel;
import ca.csf.mobile1.tp1.views.ChemicalConsoleView;

/**
 * Created by Jeammy on 24/02/17.
 */
public class ChemicalController implements ChemicalConsoleView.Listener{
    private final ChemicalConsoleView view;
    private  final ChemicalModel model;

    public ChemicalController(){
        this.view =  null;
        this.model = null;
    }

    public ChemicalController(ChemicalConsoleView view, ChemicalModel model){
        this.view = view;
        this.model = model;
        view.addListener(this);
    }

    /**
     * Action lorsqu'une donnee est entree.
     */
    @Override
    public void onDataEntered(){
        double result = 0;
        try{
            //Appel de la fonction pour calculer la masse.
            result = model.calculate(view.getInput());
        }catch (Exception e){
            view.showExceptions(e);
        }finally {
            view.setOutput(Double.toString(result));
        }
    }
    //Afficher dans la view.
    public void show(){
        view.show();
    }
}
