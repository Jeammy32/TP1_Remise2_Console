package ca.csf.mobile1.tp1.models;

import ca.csf.mobile1.tp1.chemical.compound.ChemicalCompoundFactory;
import ca.csf.mobile1.tp1.chemical.element.ChemicalElement;
import ca.csf.mobile1.tp1.chemical.element.ChemicalElementRepository;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by Jeammy on 24/02/17.
 */
public class ChemicalModel {
    private ChemicalElementRepository chemicalElementRepository;
    private ChemicalCompoundFactory chemicalCompoundFactory;

    public ChemicalModel()throws IOException{
        chemicalElementRepository = new ChemicalElementRepository();
        chemicalCompoundFactory = new ChemicalCompoundFactory(chemicalElementRepository);

        //Lecture du fichier .txt "chemicalElementsForTests.txt"
        InputStream is = this.getClass().getClassLoader().getResourceAsStream("chemicalElements.txt");
        BufferedReader br = new BufferedReader(new InputStreamReader(is));

        try {
            String line=",";
            while(line != null) {
                //valeur du char actif de la ligne
                int value;
                //information a soutirer de la ligne
                String name = new String();
                String symbol = new String();
                int number=0;
                String numberString = new String();
                double weight=0;
                String weightString = new String();

                //premiere info: le nom de l'element
                while ((char)(value = br.read()) != ',') {
                    char c = (char) value;
                    name += c;
                }
                //deuxieme info: le symbole
                while ((char)(value = br.read()) != ',') {
                    char c = (char) value;
                    symbol += c;
                }
                //troisieme info: son numero
                while ((char)(value = br.read()) != ',') {
                    char c = (char) value;
                    numberString += c;
                }
                number = Integer.parseInt(numberString);

                //quatrieme info: son poid
                while ((char)(value = br.read()) != '\r' && value != -1) {
                    char c = (char) value;
                    weightString += c;
                }
                weight = Double.parseDouble(weightString);

                //saut de ligne.
                line=br.readLine();
                //creation de l'element et ajout dans le repository
                ChemicalElement element = new ChemicalElement(name,symbol,number,weight);
                chemicalElementRepository.add(element);
            }
        }catch (Exception e)
        {
            throw new IOException();
        }
        finally {
            br.close();
            is.close();
        }
    }

    /**
     * Utilise la librairie fait dans la partie 1 du TP1 pour calculer la masse.
     * @param string : la string a evaluer.
     * @return un Double représentant le poid de l'element.
     * @throws Exception : erreur possible de la string.
     */
    public Double calculate(String string)throws Exception{
        return chemicalCompoundFactory.createFromString(string).getWeight();
    }
}
