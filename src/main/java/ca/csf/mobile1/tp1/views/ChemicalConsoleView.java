package ca.csf.mobile1.tp1.views;

import java.util.LinkedList;
import java.util.Scanner;

/**
 * Created by Jeammy on 24/02/17.
 */
public class ChemicalConsoleView {
    private final LinkedList<Listener> listeners;
    String input;
    String output;

    /**
     * Liste des controleurs
     */
    public ChemicalConsoleView(){
        listeners = new LinkedList<Listener>();
    }

    public interface Listener{
        void onDataEntered();
    }

    /**
     * Ajouter un controleur.
     * @param listener
     */
    public void addListener(Listener listener){
        listeners.add(listener);
    }

    /**
     * Ecrire a la console.
     * @param s : String a la console.
     */
    private void writeToConsole(String s){
        System.out.println(s);
    }

    /**
     * Lire de la console et avertir les listeners.
     */
    private void readFromConsole(){
        Scanner scan = new Scanner(System.in);
        input = scan.next();
        for (Listener listener : listeners){
            listener.onDataEntered();
        }
    }

    /**
     * Afficher a la console.(main)
     */
    public void show(){
        while(input != "q")
        {
            System.out.println("Entrez une formule chimique :");
            readFromConsole();
            if(Double.parseDouble(output) != 0.0D) {
                System.out.println(String.format("Le poids de %s est %s g/mol.\n", input, output));
            }
        }

    }

    /**
     * Obtenir a l'exterieur de la view l'input capture par la console.
     * @return
     */
    public String getInput(){
        return input;
    }

    /**
     * modifier le output qui sera affiche a la console a l'exterieur de la view.
     * @param s
     */
    public void setOutput(String s){
        output = s;
    }

    /**
     * Afficher les possibles exceptions.
     * @param e : l'exception a afficher.
     */
    public void showExceptions(Exception e){
        System.out.println(e.getMessage());
    }

}
