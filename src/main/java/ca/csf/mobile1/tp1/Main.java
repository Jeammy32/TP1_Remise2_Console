package ca.csf.mobile1.tp1;

import ca.csf.mobile1.tp1.controllers.ChemicalController;
import ca.csf.mobile1.tp1.models.ChemicalModel;
import ca.csf.mobile1.tp1.views.ChemicalConsoleView;

public class Main {

    public static void main(String[] args) throws Exception {
        //TODO Créer les objets du modèle
        //TODO Créer la vue
        //TODO Créer le controleur
        ChemicalModel model = new ChemicalModel();
        ChemicalConsoleView view = new ChemicalConsoleView();
        ChemicalController controller = new ChemicalController(view,model);

        view.show();
    }

}
